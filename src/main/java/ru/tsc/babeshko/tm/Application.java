package ru.tsc.babeshko.tm;

import ru.tsc.babeshko.tm.api.ICommandRepository;
import ru.tsc.babeshko.tm.constant.ArgumentConst;
import ru.tsc.babeshko.tm.constant.TerminalConst;
import ru.tsc.babeshko.tm.model.Command;
import ru.tsc.babeshko.tm.repository.CommandRepository;
import ru.tsc.babeshko.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final Boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    public static void showErrorArgument(final String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    public static void showErrorCommand(final String arg) {
        System.err.printf("Error! This command '%s' not supported...\n", arg);
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    private static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showVersion() {
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Oleg Babeshko");
        System.out.println("Diez147@gmail.com");
    }

    private static void exit() {
        System.exit(0);
    }

}